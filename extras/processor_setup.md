A processor can interact directly with any 'named' buildings in its radius.

It can interact with units anywhere on the map via `ubind`.

Remote buildings via `ulocate`; this leverages a unit's location ability.



After building a processor:
1. Single-click the processor to select it.
1. If you want to control buildings (read their properties, start/stop conveyors, etc.):
    1. Single-click other nearby buildings that you want to control.
        1. When a building is added to a processor, its name (e.g. `conveyor1` or `factory1`) will appear above it.
        1. Adding and removing buildings can result in non-serial names.  For example, you might end up with `conveyor1` and `conveyor3` with no `conveyor2`.  This can be frustrating.
1. Click the pencil under the processor (only displays when the processor is highlighted) to enter the editor.

