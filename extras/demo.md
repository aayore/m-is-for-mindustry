
**Goal:**
Put *up to* 295 coal in a container *only when* a button is pushed.
Stop the conveyor and un-set the button when 295 coal is reached.

```ruby
set max_c 295                           # Set the desired quantity of coal as 'max_c'
sensor qty_c container1 @coal           # Query 'container1' for quantity of coal, store as 'qty_c'
op lessThan need_c qty_c max_c          # 'need_c' = 'qty_c' < 'max_c'
sensor clicked switch1 @enabled         # check to see if 'switch1' is "clicked"
op land run clicked need_c              # 'run' = 'clicked' && 'need_c'
control enabled conveyor1 run 0 0 0     # set conveyor.enabled to 'run'
control enabled switch1 run 0 0 0       # Set switch1.enabled to 'run'
```

Considerations:
- This logic will loop repeatedly
- We check the switch at the last possible minute
    - Checking earlier could miss a click if it arrives mid-loop

---

**Goal:**
Use `poly` ships to mine nearby coal and deposit it in the core.

```ruby
ubind @poly                                               # 0: Interact with all `poly` units
ulocate building core false @copper homx homy found core  # 1: Locate your core, storing the coordinates as `homx` and `homy`
sensor qty @unit @coal                                    # 2: Look at the unit's payload of coal, store as `qty`
jump 8 equal qty 30                                       # 3: Skip to line 8 if unit has 30 coal
ulocate ore core true @coal coalx coaly found building    # 4: Find the closest coal ore, store as `
ucontrol approach coalx coaly 5 0 0                       # 5: Move to within 5 tiles of the coal ore
ucontrol mine coalx coaly 0 0 0                           # 6: Start mining
end                                                       # 7: End this loop (starts loop for next unit)
ucontrol approach homx homy 2 0 0                         # 8: Move to within 5 tiles of the core
ucontrol itemDrop core 999 0 0 0                          # 9: Deposit items in core
```

**Goal:**
Use a switch to control a factory.
Display red/green depending on status.

```ruby
sensor power switch1 @enabled         # 0: Check the status of the switch
control enabled factory1 power 0 0 0  # 1: Make the factory status match the switch status
jump 5 notEqual power false           # 2: Go to the "on" commands
draw color 255 0 0 255 0 0            # 3: Select "red"
jump 6 always x false                 # 4: Skip over the "on" commands
draw color 0 255 0 255 0 0            # 5: Select "green"
draw rect 0 0 80 80 0 0               # 6: Draw an 80x80 pixel rectangle
drawflush display1                    # 7: Update the display
```

Let's punch up the display a little bit...

```ruby
sensor power switch1 @enabled         # 0: Check the status of the switch
control enabled factory1 power 0 0 0  # 1: Make the factory status match the switch status
draw clear 0 0 0 255 0 0              # 2: Clear the display
jump 7 notEqual power false           # 3: Go to the "on" commands
draw color 255 0 0 255 0 0            # 4: Select "red"
draw poly 40 40 8 38 22.5 0           # 5: Draw an octogon
jump 9 always x false                 # 6: Skip over the "on" commands
draw color 0 255 0 255 0 0            # 7: Select "green"
draw poly 40 40 99 38 0 0             # 8: Draw a "circle" (technically a 99-sided polygon)
drawflush display1                    # 9: Update the display
```

If you want to control multiple factories with one switch...
(Note that the `jump` lines have updated addresses!)

```ruby
sensor power switch1 @enabled         #  0: Check the status of the switch
control enabled factory1 power 0 0 0  #  1: Make factory1 status match the switch status
control enabled factory2 power 0 0 0  #  2: Make factory2 status match the switch status
control enabled factory3 power 0 0 0  #  3: Make factory3 status match the switch status
draw clear 0 0 0 255 0 0              #  4: Clear the display
jump 9 notEqual power false           #  5: Go to the "on" commands
draw color 255 0 0 255 0 0            #  6: Select "red"
draw poly 40 40 8 38 22.5 0           #  7: Draw an octogon
jump 11 always x false                 #  8: Skip over the "on" commands
draw color 0 255 0 255 0 0            #  9: Select "green"
draw poly 40 40 99 38 0 0             # 10: Draw a "circle" (technically a 99-sided polygon)
drawflush display1                    # 11: Update the display
```


**Goal:**
Make a timer.

`@time` is a system variable that will always return the current time in miliseconds

```ruby
set sleep 5                    # Set variable `sleep` to 5
op idiv t1 @time 1000          # Store our starting time as `t1` in seconds
op idiv t2 @time 1000          # Get the current time as `t2`
op sub dt t2 t1                # Measure the elapsed time (dt = t2 - t1)
op greaterThan slept dt sleep  # `slept` will be true if our elapsed time is greater than `sleep` (5), false otherwise
jump 2 notEqual slept true     # If we haven't slept enough, go back and measure the current time again
# 
# Do something here.  This will run at 5s intervals
# 
```


**Goal:**
Reduce code with `getlink`.

Example scenario:
- You have created a processor
- You have linked the following to the processor
    1. switch1
    2. factory1
    3. conveyor1
    4. conveyor2
    5. factory2
    6. smelter1
    7. drill1
- These will essentially be stored as a 1-indexed list of linked blocks:
    - `[ switch1, factory1, conveyor1, conveyor2, factory2, smelter1, drill1 ]`

`@links` variable
- Represents the total number of blocks linked to a processor
- In the example above, `@links` would be `7`

`getlink` command:
- Code: `getlink <varname> <number>`
- GUI: `<varname> = link# <number>`
- Using the above example:
    - `getlink myblock 3` will represent `conveyor1` with the variable `myblock`
    - `control enabled myblock 1 0 0 0` will enable `conveyor1`

We can put these together to loop through our blocks:

```ruby
sensor pwr switch1 @enabled      # Read the status of the switch
set i 0                          # Initialize the loop counter
getlink block i                  # Select block number "i"
control enabled block pwr 0 0 0  # change "enabled" of this block to match the switch
op add i i 1                     # Increment our loop counter
jump 2 lessThan i @links         # Loop back to the "getlink" line if we have not reached our total number of links
```

Compare the above code to a similar function without links or looping:

```ruby
sensor pwr switch1 @enabled          # Read the status of the switch
control switch1 enabled pwr 0 0 0    # Change "enabled" of switch1 to match the switch
control factory1 enabled pwr 0 0 0   # Change "enabled" of factory1 to match the switch
control conveyor1 enabled pwr 0 0 0  # Change "enabled" of conveyor1 to match the switch
control conveyor2 enabled pwr 0 0 0  # Change "enabled" of conveyor2 to match the switch
control factory2 enabled pwr 0 0 0   # Change "enabled" of factory2 to match the switch
control smelter1 enabled pwr 0 0 0   # Change "enabled" of smelter1 to match the switch
control drill1 enabled pwr 0 0 0     # Change "enabled" of drill1 to match the switch
```

- We already have more code in the second example (8 lines vs. 6 lines)
- If we want to control an additional block with our switch...
    - The first example will assume control of the new block automatically
    - The second example requires another line of code