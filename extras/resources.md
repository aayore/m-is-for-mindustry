[Official Mindustry Wiki](https://mindustrygame.github.io/wiki/)

[Mindustry Source Code](https://github.com/Anuken/Mindustry)

[An Overly In-Depth Logic Guide](https://www.reddit.com/r/Mindustry/comments/kfea1e/an_overly_indepth_logic_guide/) (I would argue that this is not overly in-depth, but it's still pretty good.)

[Mindustry Resource Hub](https://docs.google.com/spreadsheets/d/1ck3DS1XH715DPkJnMQ2C5vmsmFL-FSSRM6j8YkLMznw/edit#gid=1152881575)

[r/Mindustry](https://www.reddit.com/r/Mindustry/)

[Unofficial Mindustry Wiki](https://mindustry-unofficial.fandom.com/wiki/Mindustry_Unofficial_Wiki)

[Mindustry Guides on Steam](https://steamcommunity.com/app/1127400/guides/)