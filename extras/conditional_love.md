# Conditional Love

Sometimes our buildings need some love.  Sometimes they don't.  Why waste power when buildings don't need love?

- Power Wasting Amateur: [Mender](https://mindustry-unofficial.fandom.com/wiki/Mender) @ 18 power / second.
- Power Wasting Pro: [Mend Projector](https://mindustry-unofficial.fandom.com/wiki/Mend_Projector) @ 90 power / second.

---

**Goal:**
Only supply power to menders when nearby buildings are damaged.

Without any kind of mender, we're generating an extra 50 power/sec.

![Power With No Mender](/labs/images/mender-power-no-mender.png)

Adding a single Mend Projecter puts us at -39 power/sec.

![Power With Mender](/labs/images/mender-power-with-mender.png)

First we'll add a processor.  The range of the hyperprocessor makes it a good choice.

![Processor For Conditional Love](/labs/images/mender-processor.png)

Now we're going to link a bunch of buildings to the processor.  All the buildings.  The number doesn't matter (AFAIK).

![Mender Processor Links](/labs/images/mender-processor-links.png)

**Important!** We have to isolate power to the Mend Projector using a diode.

![Mender Power Isolation](/labs/images/mender-power-isolation.png)

Link it!  Menders and power nodes don't have a (controllable?) `@enabled` attribute.  But a *diode* does...

![Diode Link](/labs/images/mender-processor-diode-link.png)

Now for some code...

```ruby
set i 0

main_loop:
getlink block i
sensor blockHealth block @health
sensor maxHealth block @maxHealth
jump power_on lessThan blockHealth maxHealth
op add i i 1
jump main_loop lessThan i @links
control enabled diode1 false 0 0 0
end

power_on:
control enabled diode1 true 0 0 0
```

What it's doing:

| Command | What it does |
|---|---|
| `set i 0`                                      | Initialize a loop counter |
| `main_loop:`                                   | Label for our main looping code |
| `getlink block i`                              | Use a `block` variable to represent the `i`th block/link |
| `sensor blockHealth block @health`             | Check the health of the block |
| `sensor maxHealth block @maxHealth`            | Find the maximum health of the block |
| `jump power_on lessThan blockHealth maxHealth` | If any of our blocks is below max health, turn on the mender |
| `op add i i 1`                                 | Increment our loop counter |
| `jump main_loop lessThan i @links`             | If we haven't checked all our buildings, restart the main loop |
| `control enabled diode1 false 0 0 0`           | If we have checked all our buildings and they're all healthy, turn the mender off |
| `end`                                          | And stop |
| `power_on:`                                    | Jump here to power on the mender |
| `control enabled diode1 true 0 0 0`            | Enable the diode, to provide power to the mender |

This allows you to store excess power in batteries, then only run the mender when needed.  It's also worth noting that the diode *has* to go directly between two batteries.  When we disable the diode, the "downstream" battery still has power.  The mender will continue to run until this battery is depleted.
