# Power Isolation

This [Impact Reactor schematic](#impact-reactor-schematic) is one of my favorites.  It allows you to generate huge amounts of power (each reactor @ 6300/sec) with reasonable amounts of relatively simple resources.

![Impact Reactor Schematic](/labs/images/power-impact-schematic.png)

But it comes with some caveats:

- It needs a jump start.  You have to have power available to get the reactors going.  (The reactors become self-sustaining once they're producing 1500 power/sec.)
- They shut down when overloaded.  Which is really tricky combined with the above caveat.

#### The Strategy

1. Isolate and protect batteries to jump start the reactors.
1. Isolate our output to mitigate overload.

#### Diode?

A diode allows you to control the flow of power.  At a basic level, it looks like this:

![Basic Diode Example](/labs/images/diode-example.png)

But that doesn't really demonstrate the benefits of a diode.

Look at this example.  In our ideal state, everything is powered on and running happily:

![Diode Balanced Example](/labs/images/diode-balanced.png)

But here's where a diode helps us out.  When we overload the power grid on the consumer side, our diode stops delivering power to the consumer.

![Diode Imbalanced Example](/labs/images/diode-imbalanced.png)

In this case, our Airblast Drill keeps running.  Without that diode - it's replaced with another battery below - the drill shuts off, and nothing on our grid has enough power.

![No Diode](/labs/images/diode-imbalanced-no-diode.png)

#### Protection

Plastanium walls block automatic power connections.  So when you're trying to be intentional about your power connections, plastanium fences are your friends:

![Plastanium Fence](/labs/images/power-plastanium.png)

#### Jump Start

Here's what's going on in the image below:

1. Because the reactors require an external power source, we need some external power.
2. This Surge Tower and single battery take input from the external power source.
3. The diode separates the "main grid" battery from the "jump start" batteries.  This allows the "jump start" batteries to accrue and store power for the initial start - or future restart - of the reactors.  This also makes sure that we always have power for the reactors.  The main grid won't leech power out of these batteries.
4. These six batteries store enough power to spin up our reactors.  When we're ready to start the reactors, we connect the Surge Tower.

![jump Starter](/labs/images/power-jump-start-labelled.png)

In the process of starting:

![Jump Starting](/labs/images/power-jump-started.png)

Started and self-sustaining (Surge Tower [#4] disconnected):

![Jump Started](/labs/images/power-self-sustaining.png)

Considerations:

- The need/sizing for jump start storage grows with our number of reactors.  A quick test showed approximately 52000 power - more than a full Large Battery - required to spin up a single reactor.  So budget two Large Batteries per reactor.
- *OR* you can mitigate the need for batteries by adding power switches to the reactors.  With the schematic we're using here, it's important to note that the supporting cast (all the things that aren't the reactors) consume some power, too.  So you still may need to scale your batteries as you scale your reactor setup.  And/or add as much of the supporting cast as you can/want to the power switches.

#### Output Isolation

We use a very similar pattern for the output isolation.  There are two main differences:

1. We don't need extra storage (here).  If we want to add extra batteries, we can do that elsewhere.  But this reactor is a (relatively) closed system, and it doesn't do much good to add additional batteries here.
2. We don't need to worry about disconnecting anything.  The input/storage batteries should be disconnected from the reactors so they can accrue the power needed for a future restart.  The output batteries can stay connected; the diode handles the power cutoff as needed.

![Power Output Isolation](/labs/images/power-output-isolation.png)

#### Surge Towers?

I like Surge Towers in this use case because they have limited connections and long range.  Whereas you might connect a Large Power Node to 15 things accidentally, you'll never connect a Surge Tower to more than two things accidentally.  The range of the towers is helpful when you're tying to isolate - sometimes geographically - your reactors.

#### Impact Reactor Schematic

I'm fairly sure I'm using an earlier version of [this schematic](https://mindustryschematics.com/schematics/63791e45cc5f1eb15629e5d7).  In addition to the benefits above, it is stackable - meaning it's easy to connect multiples together.

```
bXNjaAF4nHVUXYgcRRCu+Z/dzd1u9jZGFHMNAR/EvWzyoA+JkEQ50JB4JCQoQaV3pvd2ktnpYXZ27zYECYJCQAwqATnyYDQBhRD0wb8XQZM8xAeJElSEQ0XPp+DPgwi5P6u6b3N58Y7umqqu/qrq66oFH+6xwE54R8C9T3ZSHuTsoMBdZuxQzoPjvBkLKIWiG2RRmkcyge+mYsG7gj0uk0Sge95W32EvylnUYonMWaCPRDjBDggRsu2NxsONRoNNyRmRMYTewfZG02wvz3ORRaLLJntxzGRrzYF3GWf7ouB4N+dZjhiSkgpkpyOSkOWSPSFikWPUXpxHfU65ovGgSGMeCDZzkj3dF1mYRX3BpjJ5TFA1E2w/DwVrDrAskbZFwvakYcQTTCLjbZ5MgBvzpoi7YB59rgYVnnVkJsI6ltIXA5mB25UZZgvlGY6iLmbzTNEExeBOGlBqxryb1zvRLHqORorPeqb5hHKQDWQr7kXh0CEdZDyPcrGm+8d6SaA4HpFYQSuWM/VpjAblZhaF02I9mZG0jU9AOtEOfi+JJZaHkOue6qQc4Q3MO6wP0+9EQSbraSYD0e1S+ilxXk9kKDChIawO43XQh08LjNeL74oHcAT0n0HL0RLA1B8WGhwDFbDBslC4QDv42q0Aho2iSBcNGAHTRa0CJl2oalEDwyBnh+4SqKE3Ew22+kZADwVC+hSWwE1yIIH/BdwsimGC7a3lall05IBFVs9bXV19F9fvRgnVIqmv4/rJojLQtAFFheJbmCFsHJ7/oECqYJC1pqDRumwSyKah+o/lDyu2KKkCClNrKilrPSkbDRaWoqw+RXkH14IximpBVUt3kCviUJFkETs+2JpKm7ggTeHbGt++Gx8ZoKLpMTx9qMKcXwtjqzAGvoVDb1FGQ3UUL87Nnjt7Y1/xy0bl1C+1R6vbtm+uzO3+4uL98mfn2/yvPXOnB7P98+1LzQtv+28snFy6cqH56rz54+4tL209Y+566IHrpWc+mXz505s3dz718a+HHiy9OX7rrRPeyK3Jyjfy36+SAxfn0/2PnNhx6shH7s4b169eefaxzcX3xs8E71/6/PBv42dfuPziZ397rfj5y19/v+uDa1cXzm3788Pw8JY/rp3uEA82jPkFopbIX11BuYRyCbUlZV2mE9URBWoiosjRTDnUGb5ixNqI4g5TGyzqN2wqDwX2VQVF0S8i5CI+6m1qYwzkYphFDLSCIQC/bittUSE42DdAmBUi+BVc89gmjm6e11TzVFGtYgff9/8/ohZdGRu20oo1hmoNrBKKTQS0rHpZDwaW5Or5M8hgaoOaP1RcPX8umWzcVBe5uotc7GHlokbN06Nm6KECMqhRAzKoTvb09Ho0vf8BFrSSeQ==
```

![Stackable Reactors](/labs/images/impact-reactor-schematic-stackable.png)