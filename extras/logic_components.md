## Processors

| Name | Icon | Range | Instructions | Additional |
| ---|---|---|---|---|
| Micro Processor | ![Micro Processor](https://raw.githubusercontent.com/Anuken/Mindustry/master/core/assets-raw/sprites/blocks/logic/micro-processor.png) | 10 tiles | 120/s | |
| Logic Processor | ![Logic Processor](https://raw.githubusercontent.com/Anuken/Mindustry/master/core/assets-raw/sprites/blocks/logic/logic-processor.png) | 22 tiles | 480/s | |
| Hyper Processor | ![Hyper Processor](https://raw.githubusercontent.com/Anuken/Mindustry/master/core/assets-raw/sprites/blocks/logic/hyper-processor.png) | 42 tiles | 1500/s | Requires cryofluid cooling |


## Memory

| Name | Icon | Data Slots |
|---|---|---|
| Memory Cell | ![Memory Cell](https://raw.githubusercontent.com/Anuken/Mindustry/master/core/assets-raw/sprites/blocks/logic/memory-cell.png) | 64 |
| Memory Bank | ![Memory Bank](https://raw.githubusercontent.com/Anuken/Mindustry/master/core/assets-raw/sprites/blocks/logic/memory-bank.png) | 512 |

## Displays

| Name | Icon | Size | Commands |
|---|---|---|---|
| Message | ![Message](https://raw.githubusercontent.com/Anuken/Mindustry/master/core/assets-raw/sprites/blocks/logic/message.png) | 0 - Text on mouse-over only | `print`<br>`printflush` |
| Logic Display | ![Logic Display](https://raw.githubusercontent.com/Anuken/Mindustry/master/core/assets-raw/sprites/blocks/logic/logic-display.png) | 80x80 | `draw`<br>`drawflush` |
| Large Logic Display | ![Large Logic Display](https://raw.githubusercontent.com/Anuken/Mindustry/master/core/assets-raw/sprites/blocks/logic/large-logic-display.png) | 176x176 | `draw`<br>`drawflush` |

## Switch

| Name | Icon | Use |
|---|---|---|
| Switch | ![Switch](https://raw.githubusercontent.com/Anuken/Mindustry/master/core/assets-raw/sprites/blocks/logic/switch.png) |  `@enabled` property |

---

## *The following content works with Mindustry v6, but not with Mindustry v7*

### Instruction Set

- [RTFM Documentation](https://github.com/deltanedas/rtfm/blob/master/manuals/Logic/Instructions)

### Installing the in-game logic manual

1. Open Mindustry
1. On the main screen, click `Mods`
1. Click `+ Import Mod`
1. Click `:octocat: Import from GitHub`
1. Enter `deltanedas/rtfm` in the text box
1. Click `OK`
1. Mindustry should display a prompt, then close automatically

### Accessing the in-game logic manual

The most convenient way:

1. From within the in-game logic editor, there is a small :page_facing_up: file/document icon at the bottom.

Alternatively

1. From the Mindustry main screen, click `About`
1. At the bottom of the About page, click `Manuals`

