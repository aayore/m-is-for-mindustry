# I'm Building My Own Timer!

A **Hyper Processor** can run 1,500 commands per second.  Even though the **Micro Processor** only computes 120 commands per second, that can still be faster than we want.  We're going to make a timer to perform commands on a more controlled schedule.

### v7 Wait

Mindustry v7 introduced a `Wait` command to the language.  Which makes this lab most irrelevant, other than as an academic exercise.

---

1. Select your **Micro Processor**, then click the pencil to get to the editor.

1. On the editor screen, click the `X` at the top-right of each command until you're back to a blank window.

1. Click the `Add...` button

    ![Add Button](/labs/images/add_button.png)

1. In the menu that appears, click the `set` option.

    ![Coding Menu](/labs/images/coding_menu.png)

1. You should now see a purple `set` command in your editor that says, `result = 0`.  Change the name of the variable from `result` to `sleep`.  Change the value of the variable from `0` to `5`.  It should now read `sleep = 5` and look like this:

    ![Sleep Command](/labs/images/sleep_command.png)

1. Add an `Operation` command to calculate our `start_time`
    1. Click `Add...`
    1. Click `Operation`
        - This should create a new command in the visual editor that reads `result = a + b`
    1. Change the operation command to read `start_time = @time // 1000`
        - `@time` is a *system variable* that returns the current time in milliseconds
        - We use *integer division* (the `//`) to divide milliseconds by 1000 to get seconds
        - *Integer division* means that we're going to throw away any remainder.  With regular division, we know that `3 / 2 = 1.5`.  If your math teacher told you to round the answer, you would say that `3 / 2` rounds to `2`.  But with *integer division*, `3 // 2 = 1`.  We just throw away anything after the decimal place in our result.

1. Add another `Operation` command to calculate `now_time`, but this time by *copying* the one you created in the last step.  We want to calculate `now_time`.  (This may not make sense right now since our last command also just calculated the time, but hopefully it'll make sense in a few minutes.  Make sure to ask your teacher if you don't get it.)
    1. Click the `copy` icon between the `1` and the `X`.  
    1. Change variable name `start_time` to `now_time`
    1. The `Operation` command should read `now_time = @time // 1000`

1. You should now have two `Operation` commands in addition to your `Set` command:

    ![Set and Operations](/labs/images/set_and_ops.png)

1. Now determine how much time has elapsed between `start_time` and `now_time`:
    1. Add another `Operation` (you can create a new one or copy an existing one)
    1. Change it to: `elapsed_time = now_time - start_time`
        - Note: Our variable name - `elapsed_time` - might be too long to see in the editor.  That's okay.  Make your best joke about `elapsed_tim` and share it with the class.
        - Another note: Looking at the code right now, `elapsed_time` should be almost zero, right?  If our **Micro Processor** performs 120 calculations per second, all four of the commands run in 4/120th - or 1/30th of a second.
    
    ![Elapsed Tim](/labs/images/elapsed_tim.png)

1. Determine if we have slept long enough.
    1. Add another `Operation` - either create a new one of copy the last one.
    1. Make it read: `slept = elapsed_time > sleep`
        - If `elapsed_time` is less than `sleep` (which we set to `5`), the value `slept` will be the *boolean* `false`
        - If `elapsed_time` is greater than `sleep` (`5`), `slept` will be the *boolean* value `true`
        - *Boolean* is a type of variable that can only be `true` or `false`

    ![Slept](/labs/images/slept.png)

1. `Jump` back and recalculate `now_time` if we haven't slept long enough.
    1. Click `Add...`
    1. Click `Jump`
    1. Change the command to: `if slept == false`
        - Note: `==` is a *comparison operator* that tests to see if things are equal.  `=` is an *assignment operator* that sets a *variable* to a given *value*.
    1. Find the little white triangle on the right side of the `Jump` command, and drag this up to your `now_time` `Operation` command.

    ![Jump](/labs/images/jump.png)

1. Watch the timer
    1. Save your code updates:
        1. Click `Back` to go back to the map
        1. Select your **Micro Processor**
        1. Click the pencil/edit button to return to the editor
    1. Click the `Vars` button
    1. Watch the value for `elapsed_time`

    ![Timer](/labs/images/timer.gif)

1. Get your code
    1. From the Mindustry code editor, click `Edit...`
    1. Click `Copy to Clipboard`
    1. Open your own text editor (Notepad, Notes, Google Docs, Atom, VS Code etc.) and paste the code

    - Note: I do not know of a language/syntax highlighter for the Mindustry programming language.  Ruby is the best/closest I've found.  If you don't know what this means, ignore it.  :p

    ```ruby
    set sleep 5
    op idiv start_time @time 1000
    op idiv now_time @time 1000
    op sub elapsed_time now_time start_time
    op greaterThan slept elapsed_time sleep
    jump 2 equal slept false
    ```