# Flow Control

(This is a big lab.)

Many times, map tiles - space or real estate - become a very valuable commodity.  Especially around your **Core**. 

**Power** can also be a scarce resource.  Some maps intentionally make it difficult to power all your **Drills** and **Factories**.

We will learn to use these scarce resources more efficiently with in-game logic.

---

## Starting Point

Your map should look similar to the following.  You should be producing **Phase Fabric**.

![Lab04 Start](images/lab04_start.png)

---

## Do: Exploring the Code

The `Demo Phase Fabric Factory` included some code.  Let's check it out!

1. Find the processor

    ![Phase Factory Processor](images/phase_factory_proc.png)

1. Open the processor's code editor.  You should see this:

    ![Phase Code Starter](images/phase_code_starter.png)

1. If you click `Edit...` then `Copy to Clipboard`, and paste it into a text- or code-editor, you should see:

    ```ruby
    set sand_min 50
    sensor qty_sand container1 @sand
    op lessThan need_sand qty_sand sand_min
    control enabled conveyor1 need_sand 0 0 0
    ```

| Command | What it does |
|---|---|
| `set sand_min 50` | Create a variable called `sand_min` and set it to 50.  If we want to change the minimum amount of **Sand**, we only have to change this number. |
| `sensor qty_sand container1 @sand` | Check the quantity of **Sand** in **Container1** and store this value in the `qty_sand` variable. |
| `op lessThan need_sand qty_sand sand_min` | Create a new variable `need_sand` which will be `true` if `qty_sand` is less than `sand_min` and `false` otherwise. |
| `control enabled conveyor1 need_sand 0 0 0` | Set the `enabled` *attribute* of **Conveyor1** to the value of `need_sand`.  When `need_sand` is `true`, **Conveyor1** will run (`enabled = true`).  When `need_sand` is `false`, **Conveyor1** will stop (`enabled = false`). |

- An *attribute* is just that ... an attribute.  Objects in the game have *attributes* we can interact with.  In addition to the `enabled` attribute, blocks also have *attributes* describing their X position on the map, Y position on the map, and more.
- You can investigate by adding a `Sensor` command to your code, then clicking the pencil icon.
- If you really want to geek out about it, you can find these *attributes* in the Mindustry source code for [the logic editor](https://github.com/Anuken/Mindustry/blob/master/core/src/mindustry/logic/LAccess.java) and [buildings](https://github.com/Anuken/Mindustry/blob/master/core/src/mindustry/entities/comp/BuildingComp.java#L1888-L1918).

---

## Read:  The Problem

Right now, this processor isn't doing much.  Let's look at the **Phase Weaver**:

![Phase Weaver Info](images/phase_weaver_info.png)

- To create a single unit of **Phase Fabric**, the **Phase Weaver** requires `4` **Thorium** and `10` **Sand**.
- If you mouse-over the **Drill** producing **Sand**, you can see that it produces `4.93` units of **Sand** per second.
- If you mouse-over the **Drill** producing **Thorium**, you can see that it produces `2.88` units of **Thorium** per second.

### Trigger Warning: Math

You can skip this part if you want.  TL;DR - This system produces too much **Thorium** for the **Phase Weaver**.

- Supply
    - The **Drills** are supplying `58%` **Thorium** and `42%` **Sand**
    - `2.88 / 4.93 = 58%`
- Demand
    - The **Phase Weaver** requires `40%` **Thorium** and `60%` **Sand**
    - `4 / 10 = 40%`
- Excess
    - We only need `1.97` **Thorium**
    - `.4 * 4.93 = 1.97`
    - Our **Drills** are producing `0.91` excess **Thorium** every second
    - `2.88 - 1.97 = 0.91`

### Blocked!

If you let this system run too long...

1. The container can only hold `300` units per resource (e.g. `300` **Thorium** and `300` **Sand**)
1. When it's full of **Thorium**, the next unit of **Thorium** on the **Conveyor** is blocked
1. When this shared **Conveyor** stops, the **Phase Weaver** quickly runs out of **Sand**

![Blocked](images/blocked.png)

### Solutions

- Without Logic
    - When you have plenty of room, don't use a shared **Conveyor**.

        - Separate **Conveyor** belts for each resource
        - **Overflow Gates** redirect resources when the container is full
        - Bypass **Conveyors** take resources around the **Phase Weaver**

        ![Overflow](images/overflow.png)
    
    - What's the problem with this?
        - Requires a lot of real estate
        - We'll never stop pushing **Thorium** to the **Core**
        - Excess resources delivered to the **Core** are lost forever
        - **Power** is wasted mining resources that disappear

- With Logic
    - Shared **Conveyor** saves space
    - Disabling a **Drill** saves power

---

## Do: Restrict the **Thorium**

1. Enter the code editor
1. Add commands for **Thorium** using one of these two methods:
    - Mindustry Visual Editor
        1. Click the `copy` button for each command
    - Text or Code Editor
        1. Click `Edit...` then `Copy to Clipboard`
        1. Paste the commands in your favorite editor
        1. Make your changes
        1. Copy the block of commands from your favorite editor
        1. Click `Edit...` then `Import from Clipboard`
1. Change `sand` to `thorium` in each copied command
1. Only for the relevant **Thorium** command, change `conveyor1` to `drill1` (There's no **Conveyor** tile between the **Drill** and **Container** that would *only* stop **Thorium**...)
1. Change the value of `thorium_min` to `20` (Not strictly necessary, but this helps maintain the proper ratio.)
1. Your code should look like this:

    ```ruby
    set sand_min 50
    sensor qty_sand container1 @sand
    op lessThan need_sand qty_sand sand_min
    control enabled conveyor1 need_sand 0 0 0
    set thorium_min 20
    sensor qty_thorium container1 @thorium
    op lessThan need_thorium qty_thorium thorium_min
    control enabled drill1 need_thorium 0 0 0
    ```

    or

    ```ruby
    set sand_min 50
    set thorium_min 20
    sensor qty_sand container1 @sand
    sensor qty_thorium container1 @thorium
    op lessThan need_sand qty_sand sand_min
    op lessThan need_thorium qty_thorium thorium_min
    control enabled conveyor1 need_sand 0 0 0
    control enabled drill1 need_thorium 0 0 0
    ```

    or

    ![Thorium Pause Code](images/thorium_pause_code.png)

1. Exit the editor
1. Left-click your **Container**
1. Watch the **Thorium** inventory

---

## You are done with this lab when:

The quantity of **Thorium** in your **Container** hoovers at and just below `20`.

![Thorium Inventory](images/thorium_inventory.gif)