# Wait for it...

Seriously.

---

## Facts

We know that our **Phase Weaver** consumes `2` **Thorium** per second at its optimal rate.

With a limit of `20` **Thorium**, it would take `10` seconds to deplete our supply.

Our **Micro Processor** runs `120` commands per second, which is a bit overkill.

---

## Wait for it...

1. Add a `Wait` command to the top of your code and set it to `10` seconds.

    ```ruby
    wait 10
    set sand_min 50
    set thorium_min 20
    sensor qty_sand container1 @sand
    sensor qty_thorium container1 @thorium
    op lessThan need_sand qty_sand sand_min
    op lessThan need_thorium qty_thorium thorium_min
    control enabled conveyor1 need_sand 0 0 0
    control enabled drill1 need_thorium 0 0 0
    ```

1. Exit the editor and observe your **Thorium** inventory again.  What do you notice?

---

## You are done with this lab when:

The quantity of **Thorium** in your **Container** hoovers around `20`.

---

# Why?

This may seem silly, but with the **Drill** flickering on and off, we don't have much of a chance to utilize our **Power** elsewhere.  You can use **Batteries** as interim storage devices, but - and this is Aaron's unscientific opinion - Mindustry doesn't seem to handle short bursts of **Power** as effectively as it handles a sustained supply of **Power**.  Ensuring that the **Drill** is disabled for `10` seconds enables better **Power** utilization elsewhere.