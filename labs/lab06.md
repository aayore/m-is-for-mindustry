# Switches and Displays

**Switches** and **Displays** can be really handy.  You'll find a lot of uses for them.  This will be a simple introduction where we'll control a **Drill** with a **Switch** and create a very visible status indicator.

---

## Starting Point

1. Build the following next to each other
    - **Pneumatic Drill** (must be on a resource)
    - **Switch**
    - **Logic Display**
    - **Micro Processor**
1. Link them together
    1. Left-click the **Micro Processor**
    1. Left-click each of the other blocks you created above
1. Build a **Conveyor** to take resources from the **Drill** to the **Core**
1. You should now have something kinda like this:

    ![Lab06 Start](images/lab06_start.png)

---

## Switch it Up!

We only need two commands to set up the **Switch**:
1. `sensor` to read the `enabled` *attribute* of the **Switch**.
1. `control` to set the `enabled` attribute of the **Drill**

    ![Basic Switch](images/basic_switch.png)

    *Note: The second command may be truncated, but should read `set enabled of drill1 to power`*

1. Add these commands to your **Micro Processor** code
1. Exit the editor
1. Click your **Switch**
1. Watch what happens

*Note: You can control multiple blocks with a single switch.*

---

## Light it Up!

Let's start small.  We're going to make a blue circle.

![Blue Circle](images/blue_circle_code.png)

```ruby
sensor power switch1 @enabled
control enabled drill1 power 0 0 0
draw color 0 0 255 255 0 0
draw poly 40 40 99 40 0 0
drawflush display1
```

The copy/paste code for this includes a lot of extra zeros, so we'll examine the representation in the visual editor.

### Command Breakdowns

- `color r 0 g 0 b 255 a 255`

    | Piece | What is does |
    |---|---|
    | `color` | Think of this as picking a marker.  Or paint.  It's selecting a color without doing anything with it yet. |
    | `r 0` | Do not use any red. |
    | `g 0` | Do not use and green. |
    | `b 255` | Use as much blue as possible (`255` is the maximum). |
    | `a 255` | Use all the ink (`255` is the maximum, `128` would be about half transparent; `0` would be invisible - completely transparent) |

- `poly x 40 y 40 sides 99 radius 40 rotation 0`

    | Piece | What is does |
    |---|---|
    | `poly` | Using whatever color we have selected (see above), draw a polygon. |
    | `x 40 y 40` | A **Logic Display** is `80` pixels by `80` pixels, so `x 40 y 40` tells the command to center the polygon at the center of the **Logic Display**. |
    | `sides 99` | This is the closest we can get to a circle.  If you wanted a diamond, you would put `sides 4` (you have to `rotate 45` to make a square; see below). |
    | `radius 40` | This configures the size of the polygon.  Since `40` is half the radius of the **Display**, this will fill it in nicely. |
    | `rotation 0` | Doesn't matter much for a circle, but it's the difference between a diamond and a square! |

- `drawflush display1`
    - Like the `print` and `printflush` commands, everything we `draw` is stored in a **buffer** until we flush it out to a **Display**.

*Note: If you try to replace a larger shape with a smaller shape of the same color, you won't see it.  There is a `draw clear` command you must run before `draw`ing the smaller shape.*

---

## Make it Useful

1. Copy this code, then use the `Edit...` and `Import from Clipboard` to update your **Micro Processor** code.

    ```ruby
    sensor power switch1 @enabled
    control enabled drill1 power 0 0 0
    jump choose_green equal power true
    choose_red:
    draw color 255 0 0 255 0 0
    jump draw_circle always x false
    choose_green:
    draw color 0 255 0 255 0 0
    draw_circle:
    draw poly 40 40 8 40 0 0
    drawflush display1
    ```

    ![Red Green Code](images/red_green_code.png)

1. Exit the editor and click your **Switch**.

### Code Breakdown

| Command | What it does |
|---|---|
| `sensor power switch1 @enabled` | See above. |
| `control enabled drill1 power 0 0 0` | See above. |
| `jump choose_green equal power true` | If the power is on, go to the line that sets the color to green. |
| `choose_red:` | (Unused) label - basically just a comment. |
| `draw color 255 0 0 255 0 0` | The power is not on.  So set the color to red. |
| `jump draw_circle always x false` | Jump over the line that would set the color to green. |
| `choose_green:` | A label - allows code to jump here by name. |
| `draw color 0 255 0 255 0 0` | We can only get to this line if the power is on, so set the color to green. |
| `draw_circle:` | A label - allows code to jump here by name. |
| `draw poly 40 40 8 40 0 0` | Draw an 8-sided polygon that both fills and is centered in the **Display**. |
| `drawflush display1` | Update our **Display** with our drawing. |

*NOTE: Labels are really handy for organizing your code.  They make code much more readable in an outside-the-game editor.  BUT the in-game editor does not show the labels - it just shows the jumps as arrows.  For labels to persist, you need to manage your code in an editor.  Using the `Edit` --> `Copy to Clipboard` option will not include the labels - the jump targets will show as line numbers.*

---

## You are done with this lab when:

Clicking your **Switch** does both of the following:
- Toggles the **Drill** on and off
- Updates the **Display** to indicate whether the **Drill** is enabled

![Switch](images/switch.gif)