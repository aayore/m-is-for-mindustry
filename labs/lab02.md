# Getting a Sense for Copper

We're going to make the computer-in-a-computer (a Mindustry **Micro Processor**, in this case) check our quantities of **Copper**.

Mindustry processors run their code in a loop.  After the last command is run, it immediately starts over on the first command.

---

## The Illustrated Guide to Coding

1. Build a **Micro Processor** that can reach all your other blocks.  Make sure there is an empty map tile below your processor.

    ![Processor Build](images/processor_build.png)

1. Build a **Message** to the side of your **Micro Processor**.

    ![Message Build](images/message_build.png)

1. Click your **Micro Processor** - make sure there's a yellow circle around it - then click each building.  Also click the **Conveyor** closest to the **Drill**.

    ![Block Selection](images/block_selection.png)

1. Click your **Micro Processor**, then click the `pencil` icon beneath it.  You should now see a mostly-empty, semi-transparent overlay with the following editor controls:

    ![Editor Controls](images/editor_controls.png)

1. Copy the following code:

    ```
    sensor copper_in_core shard1 @copper
    sensor copper_in_container container1 @copper
    print "Copper in core: "
    print copper_in_core
    print "\nCopper in container: "
    print copper_in_container
    printflush message1
    ```

1. Click the `Edit...` button (if you don't see this, go back two steps)

1. Click `Import from Clipboard`.  This takes what you copied above and puts it in the Mindustry visual coding format.

1. You should now see this:

    ![Basic Sensor Code](images/basic_sensor_code.png)

1. Click `Back`, then make sure the game is not paused.

1. Hover your mouse cursor over the **Message** block:

    ![Message Output](images/message_output.png)

---

## Examining Our Code

| Command | What it's doing |
|---|---|
| `sensor copper_in_core shard1 @copper` | Sense the amount of **Copper** in the **Core** (`shard1`) and store this in variable `copper_in_core` |
| `sensor copper_in_container container1 @copper` | Sense the amount of **Copper** in the **Container** and store this in variable `copper_in_container` |
| `print "Copper in core: "`<br>`print copper_in_core`<br>`print "\nCopper in container: "`<br>`print copper_in_container` | These commands push *strings** into a *print buffer** ... We'll explore this more in a second. |
| `printflush message1` | Write (*flush*) the *print buffer* to our **Message** |

#### What is a *string*?

- It just means "text."

#### What's the difference between `"Copper in core: "` and `copper_in_core`?

- Because it starts with a quote, Mindustry knows the first one is a string.  A single word without quotes (in this case `copper_in_core`) is a *variable*.  A variable is a way to give a name to a value we don't know.  I don't know how much Copper is in our **Core**.  But I tell the computer to look and store the *value* in the *variable* `copper_in_core`.  (Also, I could name it `Fred`, and it could still store the value for "How much Copper is in the Core?")

#### What is a *print buffer*?

- It's a collection of *strings* that hasn't been printed or displayed yet.  The `print` command in Mindustry doesn't actually print.  It just adds a *string* to the *print buffer*.  So...

| Command | What it's doing |
|---|---|
| `print "Copper in core: "` | Add the string "Copper in core: " to the *print buffer*.  There's a space at the end because Mindustry isn't smart enough to know we want a space there.  We have to be super-specific. |
| `print copper_in_core` | Look up the value for `copper_in_core` (our first variable) and append the value to the *print buffer* |
| `print "\nCopper in container: "` | `\n` is basically computer-speak for pressing the return key.  So this adds a return, then "Copper in container: " to the *print buffer* |
| `print copper_in_container` | Look up the value for `copper_in_container` (our other variable) and append the value to the *print buffer* |

---

## You are done with this lab when:

You hover your mouse cursor over your **Message** block and see something like...

```
Copper in core: X
Copper in container: Y
```

Where...
`X` is gradually increasing.
`Y` will most likely be `0`.
