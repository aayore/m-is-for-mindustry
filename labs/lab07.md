# Unit Control

Sometimes - especially when starting a new map - you need to get resources quick.  And maybe they're not very accessible.  Using the resources you brought to the map, you can quickly deploy a fleet of **Poly** ships to mine **Coal** for you.

- I find managing units to be significantly more difficult than managing blocks and buildings.
- This is less "lab" and more of a starting point you can mess around with.

---

## Import This Schematic

**Demo Ridiculous Poly Factory**

```
bXNjaAF4nE1QTWgTQRj9ks1utt20SaEGD4qhIKhlD4Ig/hWlVlvJQUSwIkonO5MwursTZ3ej60FEEUQPLe1FtIpeelCkFHIJaBUFq5gcWvDnoBcVKdiggpeANc60/h2G975v3vfNvAcGtCoQc5FDYNVu4rDMAYqpFdgs8DL7mR1m9iDLZzwEAxPP4rToU+YCgGajHLE9iB45mgCD+sQxPRZwi0CbTU8GFP8pO3zqI5cGjmkxt0RCxqGtGNgekTUOqA9pTPN5wonrU2SbBeISjsST0M5KhGNOS8TETPwvhbjDOMH/FqWK7BThpsswMW3ECwSSDrU4M4ucWcTzhCSNMKa+3MGJmPN8Hkg/YhSFNkP/LTMQ5Wb+t1uAtDigqABJiAimqjFQICq4ItoCxI3sxMQVaBK0JaJIiEuRCrrQGDIriKhySLKoYC1LTPvL4oJFxJAGuq40f8q9zUZzsfkDdNGFdiGaPD3uDO9qv5BJ9QUv1n/Sn04MvKvM96Vm9imJrrH5evfgiezWKw8bw5t7D3eS2o3SjvLN6oY3I6Mdty3nTP3YrXWZxFxVv9f1ZeOj/tLA1OOd9x9kL12drXUOXe4uL/jZ8qb3tVdPlGKO8vDu2W2jWz5bQ2/D/qmP2xdm1p5fUVn57Nz3npGDH9TpXvXl88H617lDlb2vm9/G19yZTK+e7VEai9UJOn0tdz05dvF4QSQVl7lJiC6Dtgxx6bpF5tgqQFg1BMRlxBHlF8+swns=
```

Notes about this schematic:
- In **Sandbox** mode, we can use **Item Source** and **Liquid Source** to provide unlimited quantities of resources.
- In a real game, you'll have to use **Unloaders** at your **Core** to use the resources you brought

---

### The Code

```ruby
set toMine @lead
ubind @poly
ulocate building core false @copper homx homy found core
sensor qty @unit toMine
jump unload equal qty 30
ulocate ore core true toMine resx resy found building
ucontrol approach resx resy 5 0 0
ucontrol mine resx resy 0 0 0
end
unload:
ucontrol approach homx homy 2 0 0
ucontrol itemDrop core 999 0 0 0
```

### Code Breakdown

| Command | What it does |
|---|---|
| `set toMine @lead`                                         | Use a `toMine` variable so we can easily switch resource types |
| `ubind @poly`                                              | Interact with all `poly` units |
| `ulocate building core false @copper homx homy found core` | Locate your core, storing the coordinates as `homx` and `homy` |
| `sensor qty @unit toMine`                                  | Look at the unit's resource payload, store as `qty` |
| `jump unload equal qty 30`                                 | Go unload if unit has 30 resource |
| `ulocate ore core true toMine resx resy found building`    | Find the closest resource ore, store coordinates as `resx` and `resy` |
| `ucontrol approach resx resy 5 0 0`                        | Move to within 5 tiles of the resource ore |
| `ucontrol mine resx resy 0 0 0`                            | Start mining |
| `end`                                                      | End this loop - leave this unit mining (starts loop for next unit) |
| `unload:`                                                  | Label for jump target |
| `ucontrol approach homx homy 2 0 0`                        | Move to within 5 tiles of the core |
| `ucontrol itemDrop core 999 0 0 0`                         | Deposit items in core |
