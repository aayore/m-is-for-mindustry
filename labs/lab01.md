# Getting Started

We're going to use a replicated save to make sure we have a standard map.

1. Save [sfs-class-map.msav](/sfs-class-map.msav) in your Downloads folder
1. Open Mindustry
1. Load our class map:
    1. `Editor`
    1. `Import Map`
    1. Load `sfs-class-map.msav` from your Downloads folder
    1. `Back` -- You should now be back at the main menu
1. Play the class map in sandbox mode:
    1. `Play`
    1. `Custom Game`
    1. `SFS Class Map`
    1. `Sandbox`
    1. `Play`

*Note: We will be working in `Sandbox` mode, which gives you access to units and buildings you may not have unlocked yet in the `Campaign` mode.*

---

## Optional (But Recommended) Settings:

1. `ESC` --> `Settings` --> `Graphics`
    - Strongly recommended:
        - `Bridge Opacity` --> 50%
        - `Display Block Status` --> Make sure the "checkbox" (circle) is filled in
    - Suggested for your review:
        - `Draw Darkness/Lighting`
        - `Show Weather Graphics`
        - `Show Mouse Position`
1. Click `Back` or type `ESC` several times to get back to the game

---

## Starter Build

![Starter Build](images/starter_build.png)

1. Build a **Mechanical Drill** on the **Copper**
1. Build a **Container** between the **Mechanical Drill** and the **Core: Shard**
1. Build an **Unloader** *directly against* the **Container**, facing the **Core: Shard**
1. Pay attention to the direction of your **Conveyor** belts
    1. Build a **Conveyor** between the **Drill** and the **Container**
    1. Build a **Conveyor** between the **Container** and the **Core**

---

## You are done with this lab when:

You see **Copper** moving from the **Drill** through the **Container** to the **Core**.