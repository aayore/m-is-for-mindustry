# Schematics

This lab includes pre-built collections of blocks known as **schematics**.  When a processor is included in a **schematic**, its associations with other blocks - called "links" -  and code are maintained as part of the **schematic**.

*Note: Processors are linked to other blocks by relative coordinates.  This can impact you when you copy/paste.*

You can find a ton of shared **schematics** at [MindustrySchematics.com](https://www.mindustryschematics.com/)

## Importing Schematics

1. In the lower-right menu, click the clipboard icon labelled `Schematics`
1. Click the `Import Schematic...` button
1. Highlight the **schematic** code
1. Click the `Import from Clipboard` option

## Using Schematics

1. In the lower-right menu, click the clipboard icon labelled `Schematics`
1. Click the picture of the **schematic** you want to build
1. Position the **schematic**
    - Rotate with mouse/trackpad scroll
    - Hold `Ctrl` while you scroll if you want to zoom
    - Flip the **schematic** with `Z` and `X`
    - De-select the **schematic** with a right-click
1. Left-click to place it

---

## Build!

1. Your starting map should look something like this:

    ![Lab03 Start](images/lab03_start.png)

1. Using the instructions above, **import** these two **schematics**

    1. Demo Phase Fabric Factory

        ```
        bXNjaAF4nEWQT2jUQBTG3ybZzW42+yfag0Ww1YMUJKhYRClahK4XL6KeWjzMJkM7bpKJk6Tu4rkIpSiiiIInwaMoHhQKFaz1ICqePQj2oiCC2INFWVzf2xwMM/zee997M98E6jCigxGxkMPoDA/l+NkFlvDx06ythIfwUql6UPV54ikRp0JGAFAKWJsHCWhzF6tQi7Mg4a4nIz8TKTiJDJhyYxbxwMVonoMd05nuFc4WuQInFSmLRBbSyCLvSQXlLAok81GsYC1lIsKwqmTKVM+NszCGWiAuZ8J3lcxS1Bqh8JR0YyU9niR4QjXAG5TrKxEEUEfv/vx/SwD7cUMDNIACoYDRDtABgZlGEWg6AkMDoRcBDChiRBmuBslGLhtD2RyKJdAJZo4yNRvUXEI4NFjEomYiGjma1FmiYhlRqePFS937reunLO2w3Tpil3+NHF9/t752LZ6+sPtJc2e4fKhzw52dez369Orvwc8p6O7ikwfZ6szLT59XNiY33Q/nX3XPvTlpPXS2a8fW9t36uPm9uDTRb5/5+0M9unv75sbYvRcPpt7Pnuh0Fr5+W9nz7O3YqnV0b+uL82er9DyaeLzVh0sHpn29gq8gO2irSc5NMkmokGUTrBzVHDY91aTOCtAkfmXK8lwnWDlwwELY5mAwWMZ9Ry+gVM+rzeEkVvtYtek//gOi75kl
        ```

    1. Demo Power Plant

        ```
        bXNjaAF4nFWOTW6DMBBGPzBgfkpJbpB1JW7RA2RfdeGAFVkyhhrTqqfpupcs9TCLqrLs5/dpPB6c0AhkTk0ap2c9zZfr/KH95WqVC2hGvQ7eLMHMDkBh1U3bFenLa4FucXqbVDBDP3pjLc7BBOXMNvXD7N715+zR3bwZ7/ovaJfNroePmwlorXnbzNj7eQs6lq9Bq6m/a6e9CrEceIobORKCRJIgpTshhTgOue/7d9xfh+aSKqP+CBG1QEqp/J+WSBMIaiTiijWEI8w4zDjM4pciiyjZKghCDZHwTCKipncFW8Em2SRbyVaS5bEHqEtFJuggq2kQQsbIGQVDMkpGRSPV9ESg4S4PjJbxyOgIvz68RgI=
        ```
    
1. Place the `Demo Phase Fabric Factory` next to the pond.  Make sure the location is *exactly like this*:

    ![Phase Fabric Factory Build](images/phase_fabric_factory_build.png)

1. Place the Demo Power Plant on the coal.  Make sure the **Drills** cover all the coal:

    ![Power Plant Build](images/power_plant_build.png)

1. Connect the things:
    - Connect the **Pulse Conduit** to get water to the **Pneumatic Drills**
    - Build **Power Nodes** to power the **Phase Weaver**
    - Construct a **Titanium Conveyor** to transport **Phase Fabric** from the **Phase Weaver** to the **Core**

    ![Connections](images/connections.png)

---

## You are done with this lab when:

You see **Phase Fabric** moving from the **Phase Weaver** to the **Core**.

![Phase Production](images/phase_production.gif)