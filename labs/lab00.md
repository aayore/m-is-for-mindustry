# Reference

If you're new to Mindustry, you might want to keep this handy.  Please feel free to suggest additions or alterations!

---

## Terms

**Block** - An immovable object (building, wall, etc.) we can place on the map.

**Unit** - An air, land, or sea vehicle we create to do our bidding.

**Resources** - Solids or liquids we can use to create other things.

---

## Basic Controls

##### General Controls

| Key | Action |
|---|---|
| Space | Pause/unpause |
| Scroll (Mouse wheel or trackpad) | Zoom In/Out |

##### Ship Controls

| Key | Action |
|---|---|
| W | Up |
| A | Left |
| S | Down |
| D | Right |
| Left Mouse Click | Shoot<br>Mine (when clicking on ore) |
| Right Mouse Click | Stop Mining (only while mining) |

##### Constructing

- Use the menu at the bottom right
- Left-click a category (see below)
- Left-click a block
    - Right-click: De-select the block
    - Left-click: Place the block on the map
- Type `t` or click the clipboard icon in the bottom-right menu to access **schematics**

##### Mass Build

With a block selected from the menu, you can click and drag to build multiple objects

##### Mass Destroy

With no block selected, you can right-click and drag to delete multiple objects.

##### Mass Select

1. Without clicking, hold down the `F` key
1. Move your mouse to select multiple blocks
1. Let go of the `F` key when you have selected what you want

Why would you want to do this?

- Maybe you don't.  Right-click to stop or clear your selection.
- Left-click to build a new copy of your set of blocks.
- Left-click the `Save Schematic...` button at the bottom of the screen to keep a persistent (across games/maps) copy of your schematic

##### Making Connections

Bridges and power lines try to connect themselves as best they can, but they don't always connect how you want them to.  

Bridges ([phase] conveyors, conduits, Mass Drivers) can have multiple sources, but only one destination.

- Hovering over a node will show, if applicable:
    - Upstream connections in yellow
    - A downstream connection in purple
- Clicking a node will show, where applicable:
    - The selected node with a yellow box
    - The current downstream/destination node with a purple box
    - Potential downstream nodes with red boxes
- When a node is selected (in the state of having been clicked once)
    - Clicking the node again will disconnect the destination
    - Clicking the destination will disconnect the destination
    - Clicking a potential destination ("red box node") will connect that node as the destination

Power lines are non-directional and can be managed from either side.  Connection limits still apply.

- Hovering over a node will show the range of that node.
- Clicking a node will show other connected nodes with a purple diamond
- When a node is selected (in the state of having been clicked once)
    - Clicking the node again will disconnect all connections
        - If you do this on accident, it's frequently easier to delete and rebuild the node than try and rewire everything
    - Clicking a connected (purple diamond) node will remove the connection
    - Clicking an in-range, not-yet-connected, power-capable block (e.g. power node or building, but not a conveyor) will create a connection to that block

---

## Category Guide

| Category | Description |
|---|---|
| Weapons | Self-explanatory.  All the guns you can build in the game. |
| Conveyors | Tools for moving solid resources around the map. |
| Power | Generation, storage, and distribution of electricity. |
| Factories | Buildings to turn resources in to other |things.
| "Storage" | Item storage *and more**
| Mining | Generate resources.|
| Plumbing | Move and store liquids.|
| Defense | Walls.|
| Units | Build, move, and maintain your air, land, and sea |vehicles.
| Logic | Processors, memory, and displays.|

The "Storage" category also includes...
- Core upgrades (more item storage, more health)
- Menders to repair nearby damage
- Overdrive blocks to speed up nearby blocks
- Force projector (A shield. Why isn't this in Defense?)
- Shock Mine (Why isn't this in Weapons?)

![Left Categories](images/menu_left.png)
![Right Categories](images/menu_right.png)

---

## Unit and Block Information

Click the `Core Database` icon in the menu to see information about dang near everything in the game.

With a block selected from the menu, click the `?` button in the block summary.