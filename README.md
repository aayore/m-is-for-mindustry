<center><img src="mindustry.png"></center>

---

**Mindustry** is a hybrid tower-defense sandbox factory game. Create elaborate supply chains of conveyor belts to feed ammo into your turrets, produce materials to use for building, and defend your structures from waves of enemies. It is open source and runs on Linux, Mac, and Windows. (And Android and iOS, but these are not recommended for this class.)

If you play Mindustry solo, you can figure most of it out pretty easily. In this class, we're going to focus on logic programming within Mindustry, which is both obscure and extremely powerful. We can create custom behaviors for units and buildings using the in-game microprocessor and its supporting cast.

---

### Class Topics

- Working with the programming interface
- General logic concepts
- Saving logic in schematics
- Using buttons as power switches
- Using displays as status indicators
- Customizing unit behavior

---

### Prerequisites

- A Linux, Mac, or Windows laptop with Mindustry installed
- [GitLab.com](https://gitlab.com) account to access
    - [Course materials](https://gitlab.com/aayore/m-is-for-mindustry)
    - [Mattermost](https://mattermost.sofree.us)

---

### Agenda
- [Lab 0 - Quick Reference](labs/lab00.md)
- [Lab 1 - Getting Started](labs/lab01.md)
- [Lab 2 - Getting a Sense for Copper](labs/lab02.md)
- [Lab 3 - Schematics](labs/lab03.md)
- [Lab 4 - Flow Control](labs/lab04.md)
- [Lab 5 - Wait for it...](labs/lab05.md)
- [Lab 6 - Switches and Displays](labs/lab06.md)
- [Lab 7 - Unit Control](labs/lab07.md)

---

## Resources

[#mindustry](https://mattermost.sofree.us/sfs303/channels/mindustry) Channel on SFS Mattermost

[Official Mindustry Wiki](https://mindustrygame.github.io/wiki/)

[Mindustry Source Code](https://github.com/Anuken/Mindustry)

[An Overly In-Depth Logic Guide](https://www.reddit.com/r/Mindustry/comments/kfea1e/an_overly_indepth_logic_guide/) (I would argue that this is not overly in-depth, but it's still pretty good.)

[Mindustry Resource Hub](https://docs.google.com/spreadsheets/d/1ck3DS1XH715DPkJnMQ2C5vmsmFL-FSSRM6j8YkLMznw/edit#gid=1152881575)

[r/Mindustry](https://www.reddit.com/r/Mindustry/)

[Unofficial Mindustry Wiki](https://mindustry-unofficial.fandom.com/wiki/Mindustry_Unofficial_Wiki)

[Mindustry Guides on Steam](https://steamcommunity.com/app/1127400/guides/)